﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace PipelineSandbox.Library.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ExampleClass
    {
        [Fact]
        public void ReturnsOne()
        {
            ExampleClass.ReturnsOne().Should().Be(1);
        }

        [Fact]
        public void Default()
        {
            ExampleClass subject = new();
            subject.ValueA.Should().Be(0);
            subject.ValueB.Should().Be(0);
            subject.Total.Should().Be(0);
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(3, 5, 8)]
        public void Instance(int valueA, int valueB, int total)
        {
            ExampleClass subject = new(valueA, valueB);
            subject.ValueA.Should().Be(valueA);
            subject.ValueB.Should().Be(valueB);
            subject.Total.Should().Be(total);
        }
    }
}
