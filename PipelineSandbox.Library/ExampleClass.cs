﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

namespace PipelineSandbox.Library
{
    /// <summary>
    /// This is an example class for testing the CICD Pipeline Sandbox.
    /// </summary>
    public class ExampleClass
    {
        /// <summary>
        /// An example static method that returns <c>1</c>.
        /// </summary>
        /// <returns>The value <c>1</c>.</returns>
        public static int ReturnsOne() => 1;

        /// <summary>
        /// Creates an instance of the <see cref="ExampleClass"/> class
        /// with default member values.
        /// </summary>
        public ExampleClass() { }

        /// <summary>
        /// Creates an instance of the <see cref="ExampleClass"/> class
        /// with member values initialized to the specified parameter values.
        /// </summary>
        public ExampleClass(int valueA, int valueB)
        {
            this.ValueA = valueA;
            this.ValueB = valueB;
        }

        /// <summary>
        /// Gets the value of <see cref="ValueA"/>.
        /// </summary>
        public int ValueA { get; }

        /// <summary>
        /// Gets the value of <see cref="ValueB"/>.
        /// </summary>
        public int ValueB { get; }

        /// <summary>
        /// Gets the total value of <see cref="ValueA"/> and <see cref="ValueB"/>.
        /// </summary>
        public int Total => this.ValueA + this.ValueB;
    }
}
