# Vinland Solutions Pipeline Sandbox Repo

[![License](https://commoncorelibs.gitlab.io/pipelinesandbox/badge/license.svg)](https://commoncorelibs.gitlab.io/pipelinesandbox/license.html)
[![Platform](https://commoncorelibs.gitlab.io/pipelinesandbox/badge/platform.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Repository](https://commoncorelibs.gitlab.io/pipelinesandbox/badge/repository.svg)](https://gitlab.com/commoncorelibs/pipelinesandbox)
[![Releases](https://commoncorelibs.gitlab.io/pipelinesandbox/badge/releases.svg)](https://gitlab.com/commoncorelibs/pipelinesandbox/-/releases)
[![Documentation](https://commoncorelibs.gitlab.io/pipelinesandbox/badge/documentation.svg)](https://commoncorelibs.gitlab.io/pipelinesandbox/)  
[![Pipeline](https://gitlab.com/commoncorelibs/pipelinesandbox/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/pipelinesandbox/pipelines)
[![Coverage](https://gitlab.com/commoncorelibs/pipelinesandbox/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/pipelinesandbox/reports/index.html)
[![Tests](https://commoncorelibs.gitlab.io/pipelinesandbox/badge/tests.svg)](https://commoncorelibs.gitlab.io/pipelinesandbox/reports/index.html)  
[![Latest Release](https://gitlab.com/commoncorelibs/pipelinesandbox/-/badges/release.svg)](https://gitlab.com/commoncorelibs/pipelinesandbox/-/releases)
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.PipelineSandbox/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.PipelineSandbox/)

This project is for experimenting with CICD and the pipeline scripts. For more information see [CommonCoreLibs Pipeline Repo](https://gitlab.com/commoncorelibs/pipeline).
